MODEL
=====
1. Pipeline Model
	1.1 Build du model
	1.2 Tests (pytest)
	1.3 Qualité de code (flake8)
	1.4 Obtenir/Créer(Transformation) les données
	1.5 Exécuter le model
		Entrainement
		Sauvegarde du resultat(Fichier h5)
	1.6 Evaluation (metriques)
	***

pm 1: m1.h5
pm 2: m2.h5
pm 3: m3.h5
pm 4: m4.h5

Deploiement du Modele: MODEL.h5

APP
===
2. Pipeline App
	2.1 Build Image
	2.2 Tests (pytest)
	2.3 Qualité de code (flake8)
	2.4 Deploy Image
	






/path/MODEL.h5

	


