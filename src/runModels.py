
import os
import numpy as np
from PIL import Image
from google.cloud import storage
import cloudstorage as gcs

def create_bucket(bucket_name):
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    bucket.location = 'US'
    bucket = storage_client.create_bucket(bucket)
    vars(bucket)



def read_file(self, filename):
  self.response.write('Reading the full file contents:\n')

  gcs_file = gcs.open(filename)
  contents = gcs_file.read()
  gcs_file.close()
  self.response.write(contents)

def uploadFileToBucket(blob_name, file_path, bucket_name):
    try:
        storage_client = storage.Client()
        my_bucket = storage_client.get_bucket(bucket_name)
        blob = my_bucket.blob(blob_name)
        blob.upload_from_filename(file_path)
        return True
    except Exception as e:
        print(e)
        return


def load_data(my_list ,categ ,my_dir ,img_size):
    for ca in categ:
        path = os.path.join(my_dir ,ca)
        class_num = categ.index(ca)
        for img in os.listdir(path):
            img_arr =Image.open(os.path.join(path, img))
            img_arr1 = img_arr.convert("RGB")
            new_im = img_arr1.resize((img_size, img_size))
            new_img = np.asarray(new_im)
            arr = new_img.reshape((img_size, img_size, 3))
            my_list.append([arr, class_num])

dir = './data/chest_xray_tests'

test_dir  = os.path.join(dir,'test')
train_dir = os.path.join(dir,'train')
val_dir   = os.path.join(dir,'val')

categ = ['NORMAL', 'PNEUMONIA']
img_size = 150

train_list = []
test_list = []
val_list = []

load_data(train_list, categ, train_dir, img_size)
load_data(test_list, categ, test_dir, img_size)
load_data(val_list, categ, val_dir, img_size)

print('Taille de donnees de train: ', len(train_list))
print('Taille de donnees de test: ', len(test_list))
print('Taille de donnees de validation: ', len(val_list))

# Setup with GCS
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = './src/keys/iamodelpro-f5c180d6c629.json'
bucket_name = 'bucket_rxmodel'

uploadFileToBucket('test1.txt', './src/models/test1.txt', bucket_name)

