# Debug
C_DEBUG = False
# Graine aléatoire (utilisé pour l'initialisation d'un générateur de nombres pseudo-aléatoires)
C_RANDOM_SEED = 42
# Précission des valeurs numériques
C_PRECISSION = 3
# Système d'exploitation
import platform
C_OS = platform.system()
# Exécution sur Google Colab
#C_GOOGLE_COLAB = 'google.colab' in str(get_ipython())
# Répertoires
C_LOCAL_DATA = 'data_local'
C_BUCKET = 'bdeb_a62_pneumonie'
C_MAIN_PATH = 'chest_xray'
C_KEY_PATH = './keys/'
C_CREDENTIALS = 'bdeb-project-caaa42cfc235.json'

import os
from google.cloud import storage
import json
import posixpath
import imagesize
from PIL import Image
import pandas as pd

def create_bucket(p_storage_client, p_bucket, p_debug):
    """
    Create a new bucket in the US region with the standard storage
    class
    """
    # p_storage_client = Storage Client Instance
    # p_bucket = Bucket object
    # p_debug = Debug (yes or no)
    p_bucket.storage_class = 'STANDARD'
    new_bucket = p_storage_client.create_bucket(p_bucket, location='US')
    if p_debug:
        print(
            'Created bucket {} in {} with storage class {}'.format(
                new_bucket.name, new_bucket.location, new_bucket.storage_class
            )
        )
    return new_bucket

def find_blob(p_storage_client, p_bucket_name, p_blob, p_debug):
    """Find a blob in the bucket."""
    # p_storage_client = Storage Client Instance
    # p_bucket_name = Bucket name
    # p_blob =  Blob name
    # p_debug = Debug (yes or no)

    # Note: Client.list_blobs requires at least package version 1.17.0.
    blob_list = p_storage_client.list_blobs(p_bucket_name)
    flag=False
    for blob in blob_list:
        if blob.name == p_blob:
            flag=True
    if p_debug:
        if flag:
            print('Blob {} found'.format(p_blob))
    return flag

def upload_blob(p_storage_client, p_bucket_name, p_source_file_name, p_destination_blob_name, p_debug):
    """Uploads a file to the bucket."""
    # p_storage_client = Storage Client Instance
    # p_bucket_name = Bucket name
    # p_source_file_name =  Source file name
    # p_destination_blob_name = Storage object name
    # p_debug = Debug (yes or no)
    
    bucket = p_storage_client.bucket(p_bucket_name)
    blob = bucket.blob(p_destination_blob_name)
    blob.upload_from_filename(p_source_file_name)
    if(p_debug):
        print(
            f"File {p_source_file_name} uploaded to {p_destination_blob_name}."
        )

def download_blob(p_storage_client, p_bucket_name, p_source_blob_name, p_destination_file_name, p_debug):
    """Downloads a blob from the bucket."""
    # p_storage_client = Storage Client Instance
    # p_bucket_name = Bucket name
    # p_destination_file_name =  Destination file name
    # p_source_blob_name = Storage object name
    # p_debug = Debug (yes or no)

    bucket = p_storage_client.bucket(p_bucket_name)

    # Construct a client side representation of a blob.
    # Note `Bucket.blob` differs from `Bucket.get_blob` as it doesn't retrieve
    # any content from Google Cloud Storage. As we don't need additional data,
    # using `Bucket.blob` is preferred here.
    blob = bucket.blob(p_source_blob_name)
    blob.download_to_filename(p_destination_file_name)
    if(p_debug):
        print(
            "Downloaded storage object {} from bucket {} to local file {}.".format(
                p_source_blob_name, p_bucket_name, p_destination_file_name
            )
        )

def list_blobs(p_storage_client, p_bucket_name):
    """Lists all the blobs in the bucket."""
    # p_storage_client = Storage Client Instance
    # p_bucket_name = Bucket name

    # Note: Client.list_blobs requires at least package version 1.17.0.
    blobs = p_storage_client.list_blobs(p_bucket_name)

    for blob in blobs:
        print(blob.name)

def list_and_download_blobs(p_storage_client, p_bucket_name, p_tree, p_debug):
    """Lists all the blobs in the bucket."""
    # p_storage_client = Storage Client Instance
    # p_bucket_name = Bucket name

    # Note: Client.list_blobs requires at least package version 1.17.0.
    blobs = p_storage_client.list_blobs(p_bucket_name)

    for folder in p_tree:
        if p_debug:
            print('Create folder')
        os.makedirs(folder, exist_ok=True)
        
    for blob in blobs:
        if p_debug:
            print(blob.name) # chest_xray/ORIG/test/NORMAL/IM-0001-0001.jpeg
        destination_file_name = posixpath.join('.', C_LOCAL_DATA, blob.name).replace('/',os.sep)
        #destination_file_name = destination_file_name.replace('/',os.sep)
        if p_debug:
            print(destination_file_name)
        if blob.name[-1] != '/' and blob.name.startswith('chest_xray/ORIG/'):
            if p_debug:
                print('Download file')
            blob.download_to_filename(destination_file_name)

def generate_trees(p_local_data, p_main_path, p_project, p_datasets, p_features):
    v_tree_local = []
    v_tree_local.append(p_local_data)
    v_tree_local.append(p_local_data+os.sep+p_main_path)
    v_tree_local.append(p_local_data+os.sep+p_main_path+os.sep+p_project)
    for d in p_datasets:
        v_tree_local.append(p_local_data+os.sep+p_main_path+os.sep+p_project+os.sep+d)
        for f in p_features:
            v_tree_local.append(p_local_data+os.sep+p_main_path+os.sep+p_project+os.sep+d+os.sep+f)

    v_tree_blob = []
    v_tree_blob.append(p_main_path)
    v_tree_blob.append(p_main_path+'/'+p_project)
    for d in p_datasets:
        v_tree_blob.append(p_main_path+'/'+p_project+'/'+d)
        for f in p_features:
            v_tree_blob.append(p_main_path+'/'+p_project+'/'+d+'/'+f)
    
    return v_tree_local, v_tree_blob

# Used only in local test using Google Colab
#if (C_GOOGLE_COLAB):
#    drive.mount(C_GOOGLE_DRIVE_MOUNT)

f_dataconfig = open('./conf/dataconfig.json')
v_dataconfig = json.load(f_dataconfig)
f_dataconfig.close()

#storage_client = storage.Client.from_service_account_json(C_KEY_PATH + C_CREDENTIALS)
storage_client = storage.Client.from_service_account_json(os.environ["GOOGLE_APPLICATION_CREDENTIALS"])
bucket_data = storage_client.bucket(C_BUCKET)
try:
    if bucket_data.exists():
        bucket_data = storage_client.get_bucket(C_BUCKET)
    else:
        bucket_data = create_bucket(storage_client, bucket_data, C_DEBUG)
except:
    if C_DEBUG:
        print('Error: DATA bucket not available!')
    raise Exception('Error: DATA bucket not available!')

if find_blob(storage_client, C_BUCKET, C_MAIN_PATH + '/ORIG/', C_DEBUG):
    print('Blob ORIG found')
    blob_orig_path = posixpath.join(C_MAIN_PATH, 'ORIG') 
    blob_orig_train_path = posixpath.join(blob_orig_path, 'train') 
    blob_orig_test_path  = posixpath.join(blob_orig_path, 'test') 
    blob_orig_val_path   = posixpath.join(blob_orig_path, 'val')
    local_orig_path = os.path.join('.', C_LOCAL_DATA, C_MAIN_PATH, 'ORIG')
    local_orig_train_path = os.path.join(local_orig_path, 'train') 
    local_orig_test_path  = os.path.join(local_orig_path, 'test') 
    local_orig_val_path   = os.path.join(local_orig_path, 'val')

    #list_blobs(storage_client, C_BUCKET)
    v_tree_local_orig, v_tree_blob_orig = generate_trees(C_LOCAL_DATA, C_MAIN_PATH, 'ORIG', ['test','train','val'], ['NORMAL','PNEUMONIA'])
    if not os.path.isdir(C_LOCAL_DATA+'/'+C_MAIN_PATH+'/ORIG'):
        list_and_download_blobs(storage_client, C_BUCKET, v_tree_local_orig, C_DEBUG)
    
    #print(v_dataconfig['dataconfig'])
    for i in v_dataconfig['dataconfig']['datasets']:
        if C_DEBUG:
            print(i)
        C_PROJECT = i['name']
        C_HEIGHT  = i['height']
        C_WIDTH   = i['width']
        C_TYPE    = i['type']
        if i['force'] == 'yes':
            C_FORCE   = True
        else:
            C_FORCE   = False       

        # Define the folders
        blob_path = posixpath.join(C_MAIN_PATH, C_PROJECT) 
        blob_train_path = posixpath.join(C_MAIN_PATH, C_PROJECT, 'train')
        blob_test_path  = posixpath.join(C_MAIN_PATH, C_PROJECT, 'test') 
        blob_val_path   = posixpath.join(C_MAIN_PATH, C_PROJECT, 'val')
        local_path = os.path.join('.', C_LOCAL_DATA, C_MAIN_PATH, C_PROJECT)
        local_train_path = os.path.join('.', C_LOCAL_DATA, C_MAIN_PATH, C_PROJECT, 'train') 
        local_test_path  = os.path.join('.', C_LOCAL_DATA, C_MAIN_PATH, C_PROJECT, 'test') 
        local_val_path   = os.path.join('.', C_LOCAL_DATA, C_MAIN_PATH, C_PROJECT, 'val')

        if C_DEBUG:
            print("LOCAL:")
            print(local_orig_train_path)
            print(local_orig_test_path)
            print(local_orig_val_path)
            print(local_train_path)
            print(local_test_path)
            print(local_val_path)
            print("BLOB:")
            print(blob_orig_train_path)
            print(blob_orig_test_path)
            print(blob_orig_val_path)
            print(blob_train_path)
            print(blob_test_path)
            print(blob_val_path)        

        if not find_blob(storage_client, C_BUCKET, C_MAIN_PATH + '/' + C_PROJECT + '/', C_DEBUG) or C_FORCE:
            v_tree_local, v_tree_blob = generate_trees(C_LOCAL_DATA, C_MAIN_PATH, C_PROJECT, ['test','train','val'], ['NORMAL','PNEUMONIA'])
            for folder in v_tree_local:
                if C_DEBUG:
                    print('Create folder')
                os.makedirs(folder, exist_ok=True)        

            for dirname, _, filenames in os.walk(local_orig_path):
                for filename in filenames:
                    d = dirname.split(os.sep)
                    f = filename.split('.')
                    full = os.path.join(dirname, filename)
                    if C_DEBUG:
                        print(full)
                    print(full.replace('ORIG',C_PROJECT))
                    v_image = Image.open(full)
                    v_image = v_image.resize((C_WIDTH,C_HEIGHT))
                    v_image = v_image.convert(C_TYPE)
                    v_image.save(full.replace('ORIG',C_PROJECT))

            for dirname, _, filenames in os.walk(local_path):
                for filename in filenames:
                    d = dirname.split(os.sep)
                    f = filename.split('.')
                    full = os.path.join(dirname, filename)
                    upload_blob(storage_client, C_BUCKET, full, posixpath.join(C_MAIN_PATH, C_PROJECT, d[4], d[5], filename), C_DEBUG)
                    print('.', end='')
else:
    if C_DEBUG:
        print('Error: Blob ORIG not found!')
    raise Exception('Error: Blob ORIG not found!')

