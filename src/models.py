import os
import numpy as np
from PIL import Image
import pandas as pd

import runModels

import tensorflow as tf
from tensorflow.keras.applications import InceptionV3
from tensorflow.keras.callbacks import EarlyStopping,  ReduceLROnPlateau, TensorBoard, ModelCheckpoint
import keras

from datetime import datetime
import shutil
import matplotlib.pyplot as plt

#from keras import layers
#import visualkeras

#from tensorflow.keras.models import Sequential
#from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D
#from tensorflow.keras.applications import EfficientNetB0

#from glob import glob

#import matplotlib.pyplot as plt
#import seaborn as sns



print(" Library versions installed... ")
print(tf.__version__)
print(np.__version__)
print(Image.__version__)
print(keras.__version__)

img_size=150
categ = ['NORMAL', 'PNEUMONIA']

train_list = []
test_list = []
val_list = []

dir = './data/chest_xray'

test_dir  = os.path.join(dir,'test')
train_dir = os.path.join(dir,'train')
val_dir   = os.path.join(dir,'val')

def load_data(my_list,categ,my_dir,img_size):
    for ca in categ:
        path = os.path.join(my_dir,ca)
        class_num = categ.index(ca)
        for img in os.listdir(path):
            img_arr =Image.open(os.path.join(path,img)) 
            img_arr1 = img_arr.convert("RGB")            
            new_im = img_arr1.resize((img_size,img_size))
            new_img = np.asarray(new_im)
            arr = new_img.reshape((img_size, img_size, 3))
            my_list.append([arr,class_num]) 

print(" Loading data images... ")

load_data(train_list,categ,train_dir,img_size)
load_data(test_list,categ,test_dir,img_size)
load_data(val_list,categ,val_dir,img_size)

print('Taille de donnees de train: ', len(train_list))
print('Taille de donnees de test: ',len(test_list))
print('Taille de donnees de validation: ', len(val_list))

print(" Split data images... ")
print(" Image size target: " + str(img_size))

def split_data(X,y,my_list,img_size):
    for i,j in my_list:
        X.append(i)
        y.append(j)

    X = np.array(X).reshape(-1, img_size, img_size, 3)
    
X_train = []
y_train = []
X_test = []
y_test = []
X_val = []
y_val = []
split_data(X_train, y_train, train_list, img_size)
split_data(X_test, y_test, test_list, img_size)
split_data(X_val, y_val, val_list, img_size)

X_train = np.asarray(X_train)
y_train = np.asarray(y_train)
X_test = np.asarray(X_test)
y_test = np.asarray(y_test)
X_val = np.asarray(X_val)
y_val = np.asarray(y_val)

# Verification double
print('Donnees de TRAIN')
print('Normal :', np.unique(y_train, return_counts=True)[1][0])
print('Pneumonie :',np.unique(y_train, return_counts=True)[1][1])
print('-----')
print('Donnees de TEST')
print('Normal :', np.unique(y_test, return_counts=True)[1][0])
print('Pneumonie :',np.unique(y_test, return_counts=True)[1][1])
print('-----')
print('Donnees de VALIDATION')
print('Normal :', np.unique(y_val, return_counts=True)[1][0])
print('Pneumonie :',np.unique(y_val, return_counts=True)[1][1])

print(" INCEPTIONV3 Model ... ")

reduce_lr = ReduceLROnPlateau(monitor = 'val_accuracy', 
                              factor = 0.3, 
                              patience = 2, 
                              min_delta = 0.001,
                              mode='auto',verbose=1)

tensorboard = TensorBoard(log_dir = 'logs')
checkpoint = ModelCheckpoint(dir + "/MODEL.h5",
                             monitor="val_accuracy", save_best_only=True, mode="auto", verbose=1)


early_stop = EarlyStopping(monitor = 'val_loss', patience = 10, restore_best_weights=True)



inputs = keras.Input(shape=(img_size, img_size, 3))

# data augmentation
x = tf.keras.layers.experimental.preprocessing.RandomFlip('horizontal')(inputs)
#x = tf.keras.layers.experimental.preprocessing.RandomTranslation(height_factor=0.20, width_factor=0.20)(x)
#x = tf.keras.layers.experimental.preprocessing.RandomRotation(factor=0.09)(x)

modelIn = InceptionV3(weights='imagenet',include_top = False, input_shape=(img_size, img_size, 3))

for layer in modelIn.layers:
    layer.trainable=True
    
x = modelIn.output
x = tf.keras.layers.GlobalAveragePooling2D()(x)
x = tf.keras.layers.Dense(128,activation='relu')(x)
x = tf.keras.layers.Dense(64,activation='relu')(x)
# output layer
predictions = tf.keras.layers.Dense(1,activation='sigmoid')(x)

model_InceptionV3 = tf.keras.Model(inputs=modelIn.input, outputs=predictions)

model_InceptionV3.compile(loss='binary_crossentropy',
                          optimizer = tf.keras.optimizers.Adam(),
                          metrics=['accuracy'])
              
print('Inception')
start_CNN=datetime.now()
history_InceptionV3=model_InceptionV3.fit(X_train, y_train, epochs=20, batch_size=32, validation_data=(X_test, y_test), callbacks=[tensorboard, checkpoint, reduce_lr])
model_file =  dir + "/top_model_Inception.h5"
model_file2 = dir + "/top_model_Inception2.h5"
model_InceptionV3.save(model_file2)
print('Modele sauvegarde comme h5!')
end_CNN = datetime.now()
time_CNN = end_CNN - start_CNN 
print('Le temps pour faire le fit du modele Inception:', time_CNN)
shutil.copy2(dir + "/MODEL.h5", model_file)

bucket_name = 'bucket_rxmodel'
runModels.uploadFileToBucket('model.h5', dir + "/MODEL.h5", bucket_name)


def plotLossAccuracy(history, modelName):
    plt.figure(figsize=(12, 8))

    plt.subplot(1, 2, 1)
    plt.title('Perte')
    plt.plot(history.history['loss'], label='train')
    plt.plot(history.history['val_loss'], label='test')
    plt.xlabel("Epoch")
    plt.legend()

    plt.subplot(1, 2, 2)
    plt.title('Accuracy')
    plt.plot(history.history['accuracy'], label='train')
    plt.plot(history.history['val_accuracy'], label='test')
    plt.xlabel("Epoch")
    plt.legend()
    plt.savefig('./reports/confusion_matrix_' + modelName + '.png')
    plt.show()

plotLossAccuracy(history_InceptionV3, "InceptionV3");
runModels.uploadFileToBucket('acc.png', "./reports/confusion_matrix_InceptionV3.png", bucket_name)


def score_evaluation(model):
    score = model.evaluate(X_val, y_val)
    print("Train Loss is: ", score[0])
    print("Train Accuracy is: ", score[1])

score_evaluation(model_InceptionV3)